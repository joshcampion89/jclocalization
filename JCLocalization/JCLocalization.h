//
//  JCLocalization.h
//  JCLocalization
//
//  Created by Josh Campion on 21/02/2016.
//  Copyright © 2016 Josh Campion Development. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for JCLocalization.
FOUNDATION_EXPORT double JCLocalizationVersionNumber;

//! Project version string for JCLocalization.
FOUNDATION_EXPORT const unsigned char JCLocalizationVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <JCLocalization/PublicHeader.h>


