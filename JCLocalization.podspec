Pod::Spec.new do |s|
  s.name = 'JCLocalization'
  s.version = '0.3.1'
  s.license = { :type => 'MIT', :file => 'LICENSE.txt' }
  s.summary = 'User facing text management made easy.'
  s.description = 'Manage all of your user facing strings using enums for easy access defined in a simple tsv for easy collaboration across multiple developers and platforms.'
  s.homepage = 'https://bitbucket.org/joshcampion89/jclocalization'

  # s.social_media_url = 'http://twitter.com/...'
  s.authors = { 'Josh Campion' => 'joshcampion89@gmail.com' }
  s.source = { :git => 'https://bitbucket.org/joshcampion89/jclocalization.git', :tag => s.version }

  s.ios.deployment_target = '8.0'

  s.source_files = 'JCLocalization/**/*.swift'
  s.resources = 'JCLocalization/*.strings'
  s.preserve_paths = "JCLocalization.sh"

  s.requires_arc = true
end